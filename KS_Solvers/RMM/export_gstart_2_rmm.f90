!
! Copyright (C) 2019 National Institute of Advanced Industrial Science and Technology (AIST)
!
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
!----------------------------------------------------------------------------
SUBROUTINE export_gstart_2_rmm(gstart_)
  !----------------------------------------------------------------------------
  !
  USE mp_bands_util, ONLY : gstart
  !
  IMPLICIT NONE
  !
  INTEGER, INTENT(IN) :: gstart_
  !
  gstart = gstart_
  !
END SUBROUTINE export_gstart_2_rmm
